<?php

namespace Drupal\Tests\layout_builder_plus\FunctionalJavascript;

use \Drupal\Tests\layout_builder\FunctionalJavascript\LayoutBuilderTest;

class LayoutBuilderBlockReloadTest extends LayoutBuilderTest {
  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // @todo Add test module to preprocess blocks to add a random UUID that changes on every load to their markup.

    // @todo add two blocks
  }

  public function testBlockOperations() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // @todo Go to node/1 layout URL.

    // @todo Store existing block UUID as variable for checking later.
    // @todo Store existing block random UUID as variable for checking later.

    // Add block.
    // @todo Assert second block does not exist.
    // @todo Look for link that says add block.
    // @todo Select a system block.
    // @todo Ajax wait.
    // @todo Assert first block random UUID has not changed.
    // @todo Assert second block has been added.

    // Update block.
    $this->clickContextualLink('.THE-NEW-BLOCK', 'Configure');
    $this->assertOffCanvasFormAfterWait('layout_builder_update_block');

    $page->fillField('settings[label]', 'This is the new label');
    $page->pressButton('Update');
    $assert_session->assertWaitOnAjaxRequest();
    // @todo Assert first block random UUID has not changed.
    // @todo Assert second block random UUID DID change.

    // Remove block.
    $this->clickContextualLink('.THE-NEW-BLOCK', 'Remove block');
    $this->assertOffCanvasFormAfterWait('layout_builder_remove_block');
    $assert_session->pageTextContains('Are you sure you want to remove the This is the new label block?');
    $assert_session->pageTextContains('This action cannot be undone.');
    $page->pressButton('Remove');
    $assert_session->assertWaitOnAjaxRequest();
    // @todo Assert first block random UUID has not changed.
    // @todo Assert second block has been removed.

  }

  /**
   * Waits for the specified form and returns it when available and visible.
   *
   * @param string $expected_form_id
   *   The expected form ID.
   */
  private function assertOffCanvasFormAfterWait(string $expected_form_id): void {

    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->waitForOffCanvasArea();
    $off_canvas = $this->assertSession()->elementExists('css', '#drupal-off-canvas');
    $this->assertNotNull($off_canvas);
    $form_id_element = $off_canvas->find('hidden_field_selector', ['hidden_field', 'form_id']);
    // Ensure the form ID has the correct value and that the form is visible.
    $this->assertNotEmpty($form_id_element);
    $this->assertSame($expected_form_id, $form_id_element->getValue());
    $this->assertTrue($form_id_element->getParent()->isVisible());
  }

  /**
   * {@inheritdoc}
   *
   * @todo Remove this in https://www.drupal.org/project/drupal/issues/2918718.
   */
  protected function clickContextualLink($selector, $link_locator, $force_visible = TRUE) {
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert_session */
    $assert_session = $this->assertSession();
    /** @var \Behat\Mink\Element\DocumentElement $page */
    $page = $this->getSession()->getPage();
    $page->waitFor(10, function () use ($page, $selector) {
      return $page->find('css', "$selector .contextual-links");
    });
    if (count($page->findAll('css', "$selector .contextual-links")) > 1) {
      throw new \Exception('More than one contextual links found by selector');
    }

    if ($force_visible && $page->find('css', "$selector .contextual .trigger.visually-hidden")) {
      $this->toggleContextualTriggerVisibility($selector);
    }

    $link = $assert_session->elementExists('css', $selector)->findLink($link_locator);
    $this->assertNotEmpty($link);

    if (!$link->isVisible()) {
      $button = $assert_session->waitForElementVisible('css', "$selector .contextual button");
      $this->assertNotEmpty($button);
      $button->press();
      $link = $page->waitFor(10, function () use ($link) {
        return $link->isVisible() ? $link : FALSE;
      });
    }

    $link->click();

    if ($force_visible) {
      $this->toggleContextualTriggerVisibility($selector);
    }
  }
}

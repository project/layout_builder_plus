<?php

namespace Drupal\layout_builder_plus\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\Form\UpdateBlockForm as CoreUpdateBlockForm;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\SectionStorageInterface;

class UpdateBlockForm extends CoreUpdateBlockForm {

  use LayoutBlockConfigureTrait;
  use LayoutBuilderContextTrait;
  use LayoutBuilderHighlightTrait;

  /**
   * Rebuilds the block.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response to either rebuild the layout and close the dialog, or
   *   reload the page.
   */
  protected function rebuildAndClose(SectionStorageInterface $section_storage): AjaxResponse {
    $response = $this->updateRebuildBlock($section_storage, $this->delta, $this->uuid);
    $response->addCommand(new CloseDialogCommand('#drupal-off-canvas'));
    return $response;
  }

  /**
   * Builds the block and returns it.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   * @param $delta
   * @param $uuid
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  protected function updateRebuildBlock(SectionStorageInterface $section_storage, $delta, $uuid): AjaxResponse {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand("[data-layout-block-uuid=$this->uuid]", $this->getBlockBuild($section_storage, $delta, $uuid)));
    return $response;
  }
}

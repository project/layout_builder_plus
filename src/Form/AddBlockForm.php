<?php

namespace Drupal\layout_builder_plus\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\BeforeCommand;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\Form\AddBlockForm as CoreAddBlockForm;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\SectionStorageInterface;

class AddBlockForm extends CoreAddBlockForm {

  use LayoutBlockConfigureTrait;
  use LayoutBuilderContextTrait;
  use LayoutBuilderHighlightTrait;

  /**
   * Rebuilds the block.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response to either rebuild the layout and close the dialog, or
   *   reload the page.
   */
  protected function rebuildAndClose(SectionStorageInterface $section_storage): AjaxResponse {
    $response = $this->addRebuildBlock($section_storage, $this->delta, $this->uuid);
    $response->addCommand(new CloseDialogCommand('#drupal-off-canvas'));
    return $response;
  }

  /**
   * Builds the block and creates a new 'Add block' link.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   * @param $delta
   * @param $uuid
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  protected function addRebuildBlock(SectionStorageInterface $section_storage, $delta, $uuid): AjaxResponse {
    $response = new AjaxResponse();
    $region = $this->getCurrentComponent()->getRegion();
    $response->addCommand(new BeforeCommand("[data-layout-delta=$delta] [data-region=$region] .layout-builder__add-block", $this->getBlockBuild($section_storage, $delta, $uuid)));
    return $response;
  }
}

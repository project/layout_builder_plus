<?php

namespace Drupal\layout_builder_plus\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\layout_builder\Form\RemoveBlockForm as CoreRemoveBlockForm;
use Drupal\layout_builder\SectionStorageInterface;

class RemoveBlockForm extends CoreRemoveBlockForm {

  /**
   * Rebuilds the block.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response to either rebuild the layout and close the dialog, or
   *   reload the page.
   */
  protected function rebuildAndClose(SectionStorageInterface $section_storage): AjaxResponse {
    $response = $this->removeBlock($section_storage, $this->delta, $this->uuid);
    $response->addCommand(new CloseDialogCommand('#drupal-off-canvas'));
    return $response;
  }

  /**
   * Removes the block.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  protected function removeBlock(SectionStorageInterface $section_storage): AjaxResponse {
    $response = new AjaxResponse();
    $response->addCommand(new RemoveCommand("[data-layout-block-uuid=$this->uuid]"));
    return $response;
  }
}
